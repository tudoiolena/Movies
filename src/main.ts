import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

import "./styles/styles.css";

const apiKey = import.meta.env.VITE_API_KEY;

const movieContainer: HTMLElement | null =
  document.getElementById("film-container");
const movieContainerClass: string = "col-lg-3 col-md-4 col-12 p-2";
const searchButton: HTMLButtonElement | null = document.getElementById(
  "search-submit"
) as HTMLButtonElement | null;
const searchInput: HTMLInputElement | null = document.getElementById(
  "search"
) as HTMLInputElement | null;
const filterButtons: HTMLElement | null =
  document.getElementById("button-wrapper");
const menuButton: HTMLButtonElement | null =
  document.querySelector(".navbar-toggler");
const favoriteMoviesContainer: HTMLElement | null =
  document.getElementById("favorite-movies");
const favoriteMovieContainerClass: string = "col-12 p-2";
const bannerContainer: HTMLElement | null =
  document.getElementById("random-movie");
const loadMoreButton: HTMLElement | null = document.getElementById("load-more");

const movieCategories = new Map<
  string,
  { fetchFunction: (page: number) => Promise<IMovie[]>; containerClass: string }
>([
  [
    "popular",
    {
      fetchFunction: fetchPopularMovies,
      containerClass: movieContainerClass,
    },
  ],
  [
    "upcoming",
    {
      fetchFunction: fetchUpcomingMovies,
      containerClass: movieContainerClass,
    },
  ],
  [
    "top_rated",
    {
      fetchFunction: fetchTopRatedMovies,
      containerClass: movieContainerClass,
    },
  ],
]);

let selectedCategory = movieCategories.get("popular")!;
let currentPage = 1;
interface IMovie {
  id: number;
  poster_path: string;
  overview: string;
  release_date: string;
  title: string;
  original_title: string;
}

function mapRawMovieData(rawData: IMovie): IMovie {
  return {
    id: rawData.id,
    poster_path: rawData.poster_path,
    overview: rawData.overview,
    release_date: rawData.release_date,
    title: rawData.title,
    original_title: rawData.original_title,
  };
}

async function fetchMovies<T>(path: string): Promise<T> {
  try {
    const response = await fetch(`https://api.themoviedb.org/3/${path}`);
    let data = await response.json();
    if (data.results) {
      data.results = data.results.map((rawMovie: IMovie) =>
        mapRawMovieData(rawMovie)
      );
    } else {
      data = mapRawMovieData(data);
    }
    return data;
  } catch (error) {
    throw new Error(`Error fetching data for ${path}`);
  }
}

async function fetchMovieDetails(movieId: number): Promise<IMovie> {
  const response = await fetchMovies<IMovie>(
    `movie/${movieId}?api_key=${apiKey}`
  );
  const mappedMovie = mapRawMovieData(response);
  return mappedMovie;
}

async function fetchPopularMovies(page: number) {
  const response = await fetchMovies<{ results: IMovie[] }>(
    `movie/popular?api_key=${apiKey}&page=${page}`
  );
  const mappedMovies = response.results.map((rawMovie) =>
    mapRawMovieData(rawMovie)
  );
  return mappedMovies;
}

async function fetchUpcomingMovies(page: number): Promise<IMovie[]> {
  const response = await fetchMovies<{ results: IMovie[] }>(
    `movie/upcoming?api_key=${apiKey}&page=${page}`
  );
  const mappedMovies = response.results.map((rawMovie) =>
    mapRawMovieData(rawMovie)
  );
  return mappedMovies;
}

async function fetchTopRatedMovies(page: number): Promise<IMovie[]> {
  const response = await fetchMovies<{ results: IMovie[] }>(
    `movie/top_rated?api_key=${apiKey}&page=${page}`
  );
  const mappedMovies = response.results.map((rawMovie) =>
    mapRawMovieData(rawMovie)
  );
  return mappedMovies;
}

async function searchMovies(query: string): Promise<IMovie[]> {
  const response = await fetchMovies<{ results: IMovie[] }>(
    `search/movie?api_key=${apiKey}&query=${query}`
  );
  const mappedMovies = response.results.map((rawMovie) =>
    mapRawMovieData(rawMovie)
  );
  return mappedMovies;
}

function getFavoriteMovies(): number[] {
  const favoriteMovies = localStorage.getItem("favoriteMovies");
  const parsedFavorites = favoriteMovies ? JSON.parse(favoriteMovies) : [];
  const validFavorites = parsedFavorites.filter(
    (movieId: number) => movieId !== null
  );
  return validFavorites;
}

function toggleFavoriteMovie(movieId: number): void {
  const favorites = getFavoriteMovies() || [];
  const index = favorites.indexOf(movieId);

  if (index === -1) {
    favorites.push(movieId);
  } else {
    favorites.splice(index, 1);

    const movieCardToUpdate = document.querySelector(
      `.col-lg-3.col-md-4.col-12.p-2[movie-id="${movieId}"]`
    );
    const heartIcon = movieCardToUpdate?.querySelector(".bi-heart-fill");

    const movieCardToRemove = document.querySelector(
      `.col-12.p-2[movie-id="${movieId}"]`
    );
    heartIcon?.setAttribute("fill", "#ff000078");

    if (favoriteMoviesContainer && movieCardToRemove) {
      favoriteMoviesContainer.removeChild(movieCardToRemove);
    }
  }

  localStorage.setItem("favoriteMovies", JSON.stringify(favorites));
}

function renderMovies(
  movies: IMovie[],
  container: HTMLElement | null,
  containerClasses: string = ""
): void {
  const favoriteMovies = getFavoriteMovies();
  movies.forEach((movie) => {
    const isFavorite = favoriteMovies.includes(movie.id);
    const movieCard = createMovieCard(movie, isFavorite, containerClasses);
    container?.appendChild(movieCard);
  });
}

function createReleaseDateElement(releaseDate: string): HTMLDivElement {
  const releaseDateElement: HTMLDivElement = document.createElement("div");
  releaseDateElement.className =
    "d-flex justify-content-between align-items-center";
  const small: HTMLElement = document.createElement("small");
  small.className = "text-muted";
  small.textContent = releaseDate;
  releaseDateElement.appendChild(small);
  return releaseDateElement;
}

function createHeartIcon(isFavorite: boolean): SVGElement {
  const heartIcon: SVGElement = document.createElementNS(
    "http://www.w3.org/2000/svg",
    "svg"
  );
  heartIcon.setAttribute("xmlns", "http://www.w3.org/2000/svg");
  heartIcon.setAttribute("fill", isFavorite ? "red" : "#ff000078");
  heartIcon.setAttribute("stroke", "red");
  heartIcon.setAttribute("width", "50");
  heartIcon.setAttribute("height", "50");
  heartIcon.setAttribute("class", "bi bi-heart-fill position-absolute p-2");
  heartIcon.setAttribute("viewBox", "0 -2 18 22");

  const path: SVGPathElement = document.createElementNS(
    "http://www.w3.org/2000/svg",
    "path"
  );
  path.setAttribute("fill-rule", "evenodd");
  path.setAttribute(
    "d",
    "M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
  );

  heartIcon.appendChild(path);
  return heartIcon;
}

function createMovieCard(
  movie: IMovie,
  isFavorite: boolean,
  mainContainerClasses: string
): HTMLDivElement {
  const cardContainer: HTMLDivElement = document.createElement("div");
  cardContainer.className = mainContainerClasses;
  cardContainer.setAttribute("movie-id", `${movie.id}`);

  const card: HTMLDivElement = document.createElement("div");
  card.className = "card shadow-sm";

  const image: HTMLImageElement = document.createElement("img");
  image.src = `https://image.tmdb.org/t/p/original/${movie.poster_path}`;
  card.appendChild(image);

  const heartIcon: SVGElement = createHeartIcon(isFavorite);
  card.appendChild(heartIcon);

  const cardBody: HTMLDivElement = document.createElement("div");
  cardBody.className = "card-body";

  const description: HTMLParagraphElement = document.createElement("p");
  description.className = "card-text truncate";
  description.textContent = movie.overview;

  const releaseDate: HTMLDivElement = createReleaseDateElement(
    movie.release_date
  );

  cardBody.appendChild(description);
  cardBody.appendChild(releaseDate);
  card.appendChild(cardBody);
  cardContainer.appendChild(card);

  heartIconClickListener(heartIcon, movie.id);
  return cardContainer;
}

function heartIconClickListener(heartIcon: SVGElement, movieId: number): void {
  heartIcon.addEventListener("click", () => {
    const currentFill = heartIcon.getAttribute("fill");
    heartIcon.setAttribute(
      "fill",
      currentFill === "#ff000078" ? "red" : "#ff000078"
    );
    toggleFavoriteMovie(movieId);
  });
}

async function renderCategory(
  category: {
    fetchFunction: (page: number) => Promise<IMovie[]>;
    containerClass: string;
  },
  page: number
) {
  const movies = await category.fetchFunction(page);
  renderMovies(movies, movieContainer as HTMLElement, category.containerClass);
}

document.addEventListener("DOMContentLoaded", async () => {
  if (movieContainer) movieContainer.innerHTML = "";
  renderCategory(selectedCategory, currentPage);
});

searchButton?.addEventListener("click", async () => {
  if (movieContainer) movieContainer.innerHTML = "";
  const trimmedValue = searchInput?.value.trim();

  if (trimmedValue !== "") {
    const searchResults = await searchMovies(trimmedValue!);
    renderMovies(searchResults, movieContainer, movieContainerClass);
  } else {
    renderCategory(selectedCategory, currentPage);
  }
});

if (filterButtons) {
  const radioButtons =
    filterButtons.querySelectorAll<HTMLInputElement>(".btn-check");

  radioButtons.forEach((radioButton) => {
    radioButton.addEventListener("click", async () => {
      if (movieContainer) movieContainer.innerHTML = "";
      currentPage = 1;
      const clickedId = radioButton.id;
      const category = movieCategories.get(clickedId);

      if (category) {
        selectedCategory = category;
        renderCategory(category, currentPage);
      }
    });
  });
}

if (menuButton) {
  menuButton.addEventListener("click", async () => {
    if (favoriteMoviesContainer) favoriteMoviesContainer.innerHTML = "";
    const favoriteMoviesIDArray = getFavoriteMovies();
    const favouriteMoviesArray: IMovie[] = [];

    for (const movieId of favoriteMoviesIDArray) {
      const movie = await fetchMovieDetails(movieId);
      favouriteMoviesArray.push(movie);
    }

    renderMovies(
      favouriteMoviesArray,
      favoriteMoviesContainer as HTMLElement,
      favoriteMovieContainerClass
    );
  });
}

async function getRandomMovie(): Promise<IMovie> {
  const popularMovies = await fetchPopularMovies(1);
  const randomIndex = Math.floor(Math.random() * popularMovies.length);
  return popularMovies[randomIndex];
}

async function updateBannerContent(): Promise<void> {
  const randomMovie = await getRandomMovie();
  const bannerNameElement = document.getElementById("random-movie-name");
  const bannerDescriptionElement = document.getElementById(
    "random-movie-description"
  );

  if (bannerNameElement && bannerDescriptionElement && bannerContainer) {
    bannerNameElement.textContent = randomMovie.title;
    bannerDescriptionElement.textContent = randomMovie.overview;
    bannerContainer.style.backgroundImage = `url(https://image.tmdb.org/t/p/original/${randomMovie.poster_path})`;
    bannerContainer.style.backgroundPosition = "center";
    bannerContainer.style.backgroundRepeat = "no-repeat";
    bannerContainer.style.backgroundSize = "cover";
  }
}

document.addEventListener("DOMContentLoaded", updateBannerContent);

if (loadMoreButton) {
  loadMoreButton.addEventListener("click", async () => {
    currentPage++;

    if (filterButtons) {
      const radioButtons =
        filterButtons.querySelectorAll<HTMLInputElement>(".btn-check");

      const selectedCategory = Array.from(radioButtons).find(
        (radioButton) => radioButton.checked
      );

      if (selectedCategory) {
        const clickedId = selectedCategory.id;
        const category = movieCategories.get(clickedId);

        if (category) {
          renderCategory(category, currentPage);
        }
      }
    }
  });
}
